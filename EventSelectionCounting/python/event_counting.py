from AnalysisAlgorithmsConfig.ConfigBlock import ConfigBlock

class EventCountingBlock(ConfigBlock):
    def __init__(self, count_id, counts_output_path) -> None:
        super().__init__()
        self.count_id = count_id
        self.counts_output_path = counts_output_path

    def makeAlgs(self, config) -> None:
        algorithm = config.createAlgorithm(
            "EventCounterAlg", f"EventCounterAlg_{self.count_id}"
        )
        algorithm.countID = self.count_id
        algorithm.countsOutputPath = self.counts_output_path
