#ifndef EVENT_SELECTION_COUNTER_ALG_H
#define EVENT_SELECTION_COUNTER_ALG_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "SelectionHelpers/SysReadSelectionHandle.h"
#include "SelectionHelpers/ISelectionNameSvc.h"
#include "SystematicsHandles/SysReadHandle.h"
#include "xAODEventInfo/EventInfo.h"

#include <atomic>

// This algorithm is modelled after
// [EventCutFlowHistAlg.cxx](https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/Algorithms/AsgAnalysisAlgorithms/Root/EventCutFlowHistAlg.cxx)
// and writes/appends existing selection decorations to a json file in the format
// of a list of (name, count) pairs
class EventSelectionCounterAlg: public EL::AnaAlgorithm
{
private:
  std::map<std::string, unsigned long long> m_counts;

  // algorithm options
  CP::SysListHandle m_systematics_list {this};

  CP::SysReadHandle<xAOD::EventInfo> m_event_info_handle {
    this, "eventInfo", "EventInfo", "the EventInfo container to run on"};

  CP::SysReadSelectionHandleArray m_selections {
    this, "selections", {}, "the inputs to the event cutflow"};

  Gaudi::Property<std::string> m_counts_output_path {
    this, "countsOutputPath", "", "Name of output json file"};

  Gaudi::Property<bool> m_include_total_count {
    this, "includeTotalCount", true, "Include the total count in the output"};

  ServiceHandle<CP::ISelectionNameSvc> m_selection_name_svc {
    "SelectionNameSvc", "EventSelectionCutFlowAlg"};

public:
  EventSelectionCounterAlg(const std::string& name, ISvcLocator* loc);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;


private:
  std::string selection_label(std::string selection_name);
  StatusCode convert_counts_to_list(
    std::map<std::string, unsigned long long> counts,
    std::vector<std::pair<std::string, unsigned long long>>& counts_list
  );
};

#endif
