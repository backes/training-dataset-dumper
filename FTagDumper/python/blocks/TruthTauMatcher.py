from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock


@dataclass
class TruthTauMatcher(BaseBlock):
    """Matches truth taus to reco. jets and copies variables from truth taus to the
    matched reco. jets. For more details, see
    [TruthTauMatcher.cxx](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/main/FTagDumper/src/TruthTauMatcher.cxx)


    Parameters
    ----------
    truth_tau_collection : str
        The name of the truth tau collection to use. By default "TruthTaus"
    reco_jet_collection : str
        The name of the reco jet collection to which the truth taus are matched.
        By default the "jet_collection" from the config file used is taken.
    floats_to_copy : list[str], optional
        List of float variables to copy from the truth taus to the matched
        reco. jets. By default None
    doubles_to_copy : list[str], optional
        List of double variables to copy from the truth taus to the matched
        reco. jets. By default None
    ints_to_copy : list[str], optional
        List of integer variables to copy from the truth taus to the matched
        reco. jets. By default None
    uints_to_copy : list[str], optional
        List of unsigned integer variables to copy from the truth taus to the
        matched reco. jets. By default None
    ulongs_to_copy : list[str], optional
        List of unsigned long variables to copy from the truth taus to the
        matched reco. jets. By default None
    chars_to_copy : list[str], optional
        List of character variables to copy from the truth taus to the matched
        reco. jets. By default None
    particle_link_name : str, optional
        Name of the vector of links to matched particles.
    """

    truth_tau_collection: str = "TruthTaus"
    reco_jet_collection: str = None
    floats_to_copy: list[str] = None
    doubles_to_copy: list[str] = None
    ints_to_copy: list[str] = None
    uints_to_copy: list[str] = None
    ulongs_to_copy: list[str] = None
    chars_to_copy: list[str] = None
    max_delta_R: float = 0.3
    min_truth_tau_pT: float = 0.0
    particle_link_name: str = None

    def __post_init__(self):
        if self.truth_tau_collection is None or isinstance(
            self.truth_tau_collection, str
        ):
            self.truth_tau_collection = ["TruthTaus"]
            self.truth_tau_collection_name = self.truth_tau_collection[0]
        if self.reco_jet_collection is None:
            self.reco_jet_collection = self.dumper_config["jet_collection"]
        if self.floats_to_copy is None:
            self.floats_to_copy = []
        if self.doubles_to_copy is None:
            self.doubles_to_copy = []
        if self.ints_to_copy is None:
            self.ints_to_copy = []
        if self.uints_to_copy is None:
            self.uints_to_copy = []
        if self.ulongs_to_copy is None:
            self.ulongs_to_copy = []
        if self.chars_to_copy is None:
            self.chars_to_copy = []
        if self.particle_link_name is None:
            self.particle_link_name = ""

    def to_ca(self):
        ca = ComponentAccumulator()
        dr_str = f"deltaRTo{self.truth_tau_collection_name}"
        dpt_str = f"deltaPtTo{self.truth_tau_collection_name}"
        to_suffix = f"From{self.truth_tau_collection_name}"
        match_str = f"matchedTo{self.truth_tau_collection_name}"

        def to(f):
            return f + to_suffix

        ca.addEventAlgo(
            CompFactory.TruthTauMatcherAlg(
                f"{self.truth_tau_collection_name}To{self.reco_jet_collection}CopyAlg",
                recoJets=self.reco_jet_collection,
                truthTaus=self.truth_tau_collection,
                floatsToCopy={f: to(f) for f in self.floats_to_copy},
                doublesToCopy={f: to(f) for f in self.doubles_to_copy},
                intsToCopy={i: to(i) for i in self.ints_to_copy},
                uintsToCopy={i: to(i) for i in self.uints_to_copy},
                ulongsToCopy={i: to(i) for i in self.ulongs_to_copy},
                charsToCopy={i: to(i) for i in self.chars_to_copy},
                dR=dr_str,
                dPt=dpt_str,
                match=match_str,
                particleLink=self.particle_link_name,
                maxDeltaR=self.max_delta_R,
                minTruthTauPt=self.min_truth_tau_pT,
            )
        )

        return ca
