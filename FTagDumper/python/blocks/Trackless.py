from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock


@dataclass
class Trackless(BaseBlock):

    def to_ca(self):
        ca = ComponentAccumulator()
        ca.addEventAlgo(CompFactory.HitELDecoratorAlg('HitElementLinkDecorator'))
        ca.addEventAlgo(CompFactory.HitdRMinDecoratorAlg('HitTrackMindR'))
        ca.addEventAlgo(CompFactory.HitTruthDecoratorAlg('HitTruthDecorator'))

        return ca
