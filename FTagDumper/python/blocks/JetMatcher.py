from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class JetMatcher(BaseBlock):
    '''Matches jets from a single source collection to jets in a target collection. For more
    details, see
    [JetMatcherAlg.cxx](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/main/FTagDumper/src/JetMatcherAlg.cxx)
    
    Parameters
    ----------
    source_jets : list[str]
        The name of the source collections to match.
    source_name : str, optional
        The name of a singular jet in the source collection. Used for generating the variables
        `deltaRTo{source_name}` and `deltaPtTo{source_name}`. If not provided, it will be
        the same as the source collection.
    target_collection : str, optional
        The name of the target jet collection to match to. If not provided, it will be taken 
        from the dumper config.
    floats_to_copy : list[str], optional
        List of float variables to copy from the source jets to the target jets
    ints_to_copy : list[str], optional
        List of int variables to copy from the source jets to the target jets
    pt_priority_with_delta_r : float, optional
        The priority of the pt variable when matching jets based on deltaR.
        Disabled by default.
    particle_link_name : str, optional
        Name of the vector of links to matched particles.
    '''
    source_jets: list[str]
    source_name: str = None
    target_collection: str = None
    floats_to_copy: list[str] = None
    ints_to_copy: list[str] = None
    pt_priority_with_delta_r: float = -1
    source_minimum_pt: float = 0.0
    particle_link_name: str = None

    def __post_init__(self):
        if self.source_name is None:
            self.source_name = self.source_jets
        if self.target_collection is None:
            self.target_collection = self.dumper_config["jet_collection"]
        if self.floats_to_copy is None:
            self.floats_to_copy = []
        if self.ints_to_copy is None:
            self.ints_to_copy = []
        if self.particle_link_name is None:
            self.particle_link_name = ""

    def to_ca(self):
        ca = ComponentAccumulator()
        dr_str = f'deltaRTo{self.source_name}'
        deta_str = f'deltaEtaTo{self.source_name}'
        dphi_str = f'deltaPhiTo{self.source_name}'
        dpt_str = f'deltaPtTo{self.source_name}'
        to_suffix = f'From{self.source_name}'
        match_str = f'matchedTo{self.source_name}'
        def to(f):
            return f + to_suffix
        ca.addEventAlgo(
            CompFactory.JetMatcherAlg(
                f'{self.source_name}To{self.target_collection}CopyAlg',
                targetJet=self.target_collection,
                sourceJets=self.source_jets,
                floatsToCopy={f: to(f) for f in self.floats_to_copy},
                intsToCopy={i: to(i) for i in self.ints_to_copy},
                dR=dr_str,
                dEta=deta_str,
                dPhi=dphi_str,
                dPt=dpt_str,
                match=match_str,
                ptPriorityWithDeltaR=self.pt_priority_with_delta_r,
                sourceMinimumPt=self.source_minimum_pt,
                particleLink=self.particle_link_name,
            )
        )

        return ca
