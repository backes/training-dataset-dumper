from enum import Enum, auto

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory


class JC(Enum):
    PFlow = auto()
    LargeR = auto()

VARIATIONS={"JET_GroupedNP_1_UP"}

###############################################################
# Function to apply jet systematics
###############################################################

def applyJetSys(
        sys_list,
        jet_collection,
        jet_sigma,
        output_jet_collection=None):

    ca = ComponentAccumulator()

    if jet_collection.startswith("AntiKt4EMPFlowJets"):
        jc = JC.PFlow
    elif jet_collection.startswith("AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"):
        jc = JC.LargeR
    else:
        raise ValueError(f'Unsupported jet colleciton: {jet_collection}')

    jetdef = jet_collection.split('_')[0].replace("Jets", "")
    if not output_jet_collection:
        output_jet_collection = f'{jet_collection}Sys'

    jetUncertTool = CompFactory.JetUncertaintiesTool(
        name='JetUncertaintiesTool',
        ConfigFile={
            JC.PFlow: "rel22/Summer2023_PreRec/R4_SR_Scenario1_SimpleJER.config",
            JC.LargeR: "rel21/Fall2022/R10_CategoryReduction.config"
        }[jc],
        MCType={
            JC.PFlow: "MC20",
            JC.LargeR: "MC16"
        }[jc],
        IsData=False,
        JetDefinition=jetdef,
    )
    # Check the configs here:
    # /GroupData/JetUncertainties/CalibArea-08/rel21/Summer2019/. The
    # names of the systematics can be found there.  the name of the
    # jet systematics should be of this format:
    # JET_Flavor_Response.The c++ implementation is rather minimal so
    # that the user needs to ensure the correct systematic name is
    # used.
    jetCalibTool = CompFactory.JetCalibrationTool(
        ConfigFile={
            JC.PFlow: "PreRec_R22_PFlow_ResPU_EtaJES_GSC_February23_230215.config",
            JC.LargeR: "JES_MC16recommendation_R10_UFO_CSSK_SoftDrop_JMS_Insitu_30Sep2022.config"
        }[jc],
        JetCollection=jetdef,
        IsData=False,
        CalibSequence="JetArea_Residual_EtaJES_GSC"
    )

    # share the tools, if possible
    ca.addPublicTool(jetUncertTool)
    ca.addPublicTool(jetCalibTool)

    jetSysAlg = CompFactory.JetSystematicsAlg(
        name=f'JetSysAlg_{output_jet_collection}',
        jet_collection=jet_collection,
        output_jet_collection=output_jet_collection,
        systematic_variations=sys_list,
        sigma=jet_sigma,
        jet_uncert_tool=jetUncertTool,
        jet_calib_tool=jetCalibTool,
    )

    ca.addEventAlgo(jetSysAlg)
    return ca

