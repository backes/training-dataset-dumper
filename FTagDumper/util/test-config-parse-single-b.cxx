#include "src/JetDumperConfig.hh"
#include "src/ConfigFileTools.hh"

#include <nlohmann/json.hpp>
#include <stdexcept>

#include <filesystem>
#include <iostream>

const std::string CFG_GROUP = "dumper";
bool has_cfg_group(const nlohmann::ordered_json& j, std::string group) {
  if (j.count(group)) return true; 
  return false;
}

nlohmann::ordered_json* find_dumper_cfg(nlohmann::ordered_json& jsonObj) {
  /*
  Searches through the config file till it finds the main dumper config and returns 
  a pointer to it
  */
  if(has_cfg_group(jsonObj, "jet_collection")){
    return &jsonObj;
  }
  // First check if 'jet_collection' is in the jsonObj
  if (jsonObj.is_object()) {
      for (auto& [k, v] : jsonObj.items()) {
          if (has_cfg_group(v, "jet_collection")) {
              return &v; 
          }
          if (v.is_object() || v.is_array()) {
              auto* result = find_dumper_cfg(v);
              if (result) {
                  return result; 
              }
          }
      }
  } else if (jsonObj.is_array()) {
      for (auto& elem : jsonObj) {
        auto* result = find_dumper_cfg(elem);
          if (result) {
              return result;
          }
      }
  }
  return nullptr; 
}


int main(int narg, char* argv[]) {
  namespace fs = std::filesystem;
  if (narg != 2) {
    std::cout << "usage: " << argv[0] << " <json_file>" << std::endl;
    return 1;
  }

  fs::path cfg_path(argv[1]);

  if (!fs::exists(cfg_path)) return 2;
  std::ifstream cfg_stream(cfg_path);
  auto nlocfg = nlohmann::ordered_json::parse(cfg_stream);
  ConfigFileTools::combine_files(nlocfg, cfg_path.parent_path());

  auto cfg = find_dumper_cfg(nlocfg);
  if(cfg){
    cfg->erase("ca_blocks");
    get_jetdumper_config(*cfg, cfg_path.stem());
  }else{
      std::cerr << "Error: Could not find dumper config section." << std::endl;
      return 1;
  }
  return 0;
}
