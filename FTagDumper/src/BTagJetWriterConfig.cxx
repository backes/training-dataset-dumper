#include "BTagJetWriterConfig.hh"

BTagJetWriterBaseConfig::BTagJetWriterBaseConfig():
  name("")
{
}

BTagJetWriterConfig::BTagJetWriterConfig():
  BTagJetWriterBaseConfig(),
  n_jets_per_event(0)
{
}

SubjetWriterConfig::SubjetWriterConfig():
  BTagJetWriterBaseConfig(),
  n_subjets_to_save(0),
  write_kinematics_relative_to_parent(false)
{
}
