/*
  Copyright (C) 2002-2045 CERN for the benefit of the ATLAS collaboration
*/
#include "TruthJetPrimaryVertexDecoratorAlg.h"
#include "StoreGate/WriteDecorHandle.h"

TruthJetPrimaryVertexDecoratorAlg::TruthJetPrimaryVertexDecoratorAlg(
  const std::string& name, ISvcLocator* pSvcLocator):
  AthReentrantAlgorithm(name, pSvcLocator)
{
}

StatusCode TruthJetPrimaryVertexDecoratorAlg::initialize() {
  ATH_CHECK(m_decPv.initialize());
  ATH_CHECK(m_events.initialize());
  return StatusCode::SUCCESS;
}

StatusCode TruthJetPrimaryVertexDecoratorAlg::execute(
  const EventContext& cxt) const
{
  SG::ReadHandle events(m_events, cxt);
  if (!events.isValid()) {
    ATH_MSG_ERROR("invalid event handle");
    return StatusCode::FAILURE;
  }
  if (events->size() != 1) {
    ATH_MSG_ERROR("number of truth events != 1");
    return StatusCode::FAILURE;
  }

  const xAOD::TruthVertex* truth_pv = nullptr;

  if (m_useSignalProcessVertex) {

    // Access the truth jet vertex using signalProcessVertex
    truth_pv = events->at(0)->signalProcessVertex();  
  }
  else {

    // Access the truth jet vertex using the TruthJetVertices container
    const xAOD::TruthVertexContainer* truth_pv_container = nullptr;   
    if (evtStore()->retrieve(truth_pv_container, "TruthPrimaryVertices").isSuccess()) {

      // Now check that it's not empty
      if (!truth_pv_container->empty()) {
        truth_pv = truth_pv_container->at(0);
     } 
     else {
       ATH_MSG_ERROR("TruthPrimaryVertices container is empty.");
       return StatusCode::FAILURE;
     }

      // And check the size = 1
      if (truth_pv_container->size() != 1) {
       ATH_MSG_ERROR("TruthPrimaryVertices container has size = " << truth_pv_container->size() << " but should be 1");
        return StatusCode::FAILURE;
      }
    }
  }

  // If no truth_pv is found then exit
  if (!truth_pv) {
    ATH_MSG_ERROR("No truth jet vertex found");
    return StatusCode::FAILURE;
  }

  // Decorate the jet with the true jet vertex z-position
  // std::cout << "Jet origin z-position: " << truth_pv->z() << std::endl;
  SG::WriteDecorHandle<JC,float> jet_dec(m_decPv, cxt);
  for (const auto* jet: *jet_dec) {
    jet_dec(*jet) = truth_pv->z();
  }
  return StatusCode::SUCCESS;
};

