#ifndef TRACK_FLOW_OVERLAP_REMOVAL_ALG_HH
#define TRACK_FLOW_OVERLAP_REMOVAL_ALG_HH

#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "GaudiKernel/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "StoreGate/WriteDecorHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"

#include "xAODJet/JetContainer.h"
#include "AthLinks/ElementLink.h"

class TrackFlowOverlapRemovalAlg :  public AthReentrantAlgorithm {
public:

  /**< Constructors */
  TrackFlowOverlapRemovalAlg(const std::string& name, ISvcLocator *pSvcLocator);

  /**< Main routines */
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext&)  const override;

private:

  using IPC = xAOD::IParticleContainer;

  SG::ReadDecorHandleKey<IPC> m_tracksKey {
    this, "Tracks", "Something.GhostTracks",
      "Key for the input track links"};
  
  SG::ReadDecorHandleKey<IPC> m_constituentKey {
    this, "Constituents", "Something.constituentLinks",
      "Key for the input track links"};

  SG::WriteDecorHandleKey<IPC> m_tracksOutKey {
    this, "OutTracks", "Something.ThinnedGhostTracks",
    "Link to be added to the Jet"};
};

#endif
