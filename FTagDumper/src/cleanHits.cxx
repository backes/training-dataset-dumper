/*
Modules in the silicon tracker are overlapping in phi and z.
If a track passes through one of these overlap regions it will leave
two hits behind in that overlap region where normally it would have only left one hit.
The cleanHits function removes hits which are close in dPhi and dZ if they
come from different detectorElementID. The isGoodHit function removes fake hits coming
from ganged pixels and other problematic hits, reproducing as closely as possible
what is done in the PixelConditionSummaryTool
*/

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "AthContainers/AuxElement.h"
#include "cleanHits.hh"
#include "TVector3.h"
#include <limits>

namespace {
  template <typename T>
  using Acc = SG::AuxElement::ConstAccessor<T>;

// Accessors
  Acc<int> layer("layer");
  Acc<int> bec("bec");
  Acc<char> isFake("isFake");
  Acc<int> DCSState("DCSState");
  Acc<int> hasBSError("hasBSError");
  Acc<unsigned long> deid("detectorElementID");
  Acc<char> isSCT("isSCT");

  struct HitPos {
    float z;
    float phi;
    unsigned long deid;
    char layer;
  };
  float deltaPhi(const HitPos& h1, const HitPos& h2) {
    float dphi = h2.phi - h1.phi;
    if (dphi > M_PI) {
      dphi -= M_PI * 2;
    } else if (dphi <= -M_PI) {
      dphi += M_PI * 2;
    }
    return dphi;
  }

}

bool isGoodHit(const xAOD::TrackMeasurementValidation* hit) {
  if (isFake(*hit)) return false;
  if (hasBSError(*hit)) return false;
  if (DCSState(*hit)) return false;
  return true;
}


// Function that performs removal of fake and overlapping hits
std::vector<const xAOD::TrackMeasurementValidation*> cleanHits(const xAOD::TrackMeasurementValidationContainer* inHits,
                     bool removeBadHits, bool do_overlap_removal) {

  std::vector<const xAOD::TrackMeasurementValidation*> outHitsCollection;
  std::unordered_map<char,std::vector<HitPos>> savedHits;


  for (const auto *inhit : *inHits) {
    // Remove problematic hits

    if (!isSCT(*inhit) && removeBadHits && !isGoodHit(inhit)) continue;
    // Get input hit position and layer
    TVector3 inHitVec(inhit->globalX(), inhit->globalY(), inhit->globalZ());
    HitPos inHitPos;
    inHitPos.z = inHitVec.Z();
    inHitPos.phi = inHitVec.Phi();
    inHitPos.deid = deid(*inhit);
    inHitPos.layer = layer(*inhit);
    int layerIn = inHitPos.layer;
    // int layerIn = 0;

    // Is the hit in the barrel?
    bool isBarrel = bec(*inhit) == 0;

    if (do_overlap_removal) {
      // Loop over output hits and check if any hit overlaps with hit to be added
      bool overlap=false;
      if (savedHits.count(layerIn)) {
        for (const auto &outHitPos : savedHits.at(layerIn)) {
          if (inHitPos.layer != outHitPos.layer) continue;
          // Set dPhi and dZ cuts - these are empirically defined so as to make
          // doubly counted hits disappear
          float dZcut = 20.; // mm
          float dPhicut = isBarrel ? 0.004 : std::numeric_limits<float>::max();

          // Otherwise check their overlap in dPhi
          float dPhi = std::abs(deltaPhi(outHitPos, inHitPos));
          float dZ = std::abs(outHitPos.z - inHitPos.z);
          if (dPhi < dPhicut && dZ < dZcut && inHitPos.deid != outHitPos.deid) {
            overlap = true;
            break;
          }
        }
      }
      if (!overlap) {
        outHitsCollection.push_back(inhit);
        savedHits[layerIn].push_back(inHitPos);
      }
    }
    else {
      outHitsCollection.push_back(inhit);
      savedHits[layerIn].push_back(inHitPos);
    }
  }

  return outHitsCollection;

}
