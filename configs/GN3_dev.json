{
    "jet_collection": "AntiKt4EMPFlowJets",
    "calibration": {"file": "fragments/pflow-calibration.json"},
    "selection": {"file": "fragments/pflow-selection.json"},
    "vertex_collection": "PrimaryVertices",
    "truths" : [
        {"file": "fragments/overlap-leptons-TRUTH3.json"},
        {
            "association": {
                "file": "fragments/baseline-truth-kinematics.json",
                "particles": "promptLepton"
            },
            "decorate_summary": true
        }
    ],
    "decorate": {
        "jet_aug": false,
        "btag_jes": true,
        "soft_muon": false,
        "track_sv_info": true,
        "lepton_decay_label": true
    },
    "btagging_link": "btaggingLink",
    "dl2_configs": [],
    "variables": {
        "file": "fragments/pflow-variables-slim.json",
        "default_mapping": {"file": "fragments/default-mapping.json"},
        "jet" : {
            "ints" : [
                "n_tracks_dr", "n_tracks_ghost", "HadronConeExclTruthLabelIDFromNearestJet",
                "n_b_tracks_ghost",
                "n_bc_tracks_ghost",
                "n_c_tracks_ghost",
                "n_hf_tracks_ghost"
            ],
            "uints": ["jetFoldHash", "jetFoldHash_noHits", "classifierParticleOutComeFromTruthTaus"],
            "floats": [
                "ptFromTruthJet", 
                "ptFromTruthDressedWZJet", 
                "deltaPtToTruthTaus",
                "deltaRToNearestJet",
                "deltaEtaToNearestJet",
                "deltaPhiToNearestJet",
                "deltaPtToNearestJet",
                "ptFromNearestJet",
                "etaFromNearestJet",
                "phiFromNearestJet"
            ],
            "chars": ["matchedToTruthJet", "matchedToTruthDressedWZJet", "matchedToTruthTaus"]
        },
        "btagging": {
            "floats": [
                "GN2v01_pb",
                "GN2v01_pc",
                "GN2v01_pu",
                "GN2v01_ptau",
                "GN2NoAux_pb",
                "GN2NoAux_pc",
                "GN2NoAux_pu",
                "GN2NoAux_ptau",
                "GN2Lep_pb",
                "GN2Lep_pc",
                "GN2Lep_pu",
                "GN2Lep_ptau"
            ]
        }
    },
    "tracks": [
        {
            "n_to_save": 50,
            "selection": {"file": "fragments/r22loose-track-cuts.json"},
            "sort_order": "d0_significance",
            "btagging_link": "btaggingLink",
            "input_name": "BTagTrackToJetAssociator",
            "output_name": "tracks_dr",
            "variables": {
                "file": "fragments/pflow-track-variables-slim.json",
                "chars": ["GN2v01_aux_TrackOrigin", "GN2v01_aux_VertexIndex"]
            },
            "ip_prefix": "btagIp_"
        },
        {
            "n_to_save": 50,
            "selection": {"file": "fragments/r22loose-track-cuts.json"},
            "sort_order": "d0_significance",
            "input_name": "GhostTrack",
            "output_name": "tracks_ghost",
            "variables": {
                "file": "fragments/pflow-track-variables-slim.json",
                "chars": ["GN2v01_aux_TrackOrigin", "GN2v01_aux_VertexIndex"]
            },
            "ip_prefix": "btagIp_"
        }
    ],
    "flow": [
        {
            "type": "flow",
            "output_name": "flows",
            "n_to_save": 50,
            "accessor": "constituentLinks",
            "variables": {
                "customs": [
                    "pt",
                    "energy",
                    "deta",
                    "dphi",
                    "isCharged"
                ]
            }
        }
    ],
    "electrons": {
        "file": "fragments/flow_el.json",
        "input_name": "GhostFTagElectrons",
        "selection": {
            "file": "fragments/gnn-electron-cuts.json"
        }
    },
    "ca_blocks": [
        {
            "block": "JetMatcher",
            "source_jets" : ["AntiKt4TruthJets"],
            "source_name" : "TruthJet",
            "floats_to_copy" : ["pt"],
            "pt_priority_with_delta_r": 0.3
        },
        {
            "block": "JetMatcher",
            "source_jets" : ["AntiKt4TruthDressedWZJets"],
            "source_name" : "TruthDressedWZJet",
            "floats_to_copy" : ["pt"],
            "pt_priority_with_delta_r": 0.3
        },
        {
            "block": "JetMatcher",
            "source_jets" : ["AntiKt4EMPFlowJets"],
            "source_name" : "NearestJet",
            "floats_to_copy" : ["pt", "eta", "phi"],
            "ints_to_copy" : ["HadronConeExclTruthLabelID"]
        },
        {
            "block": "TruthTauMatcher",
            "truth_tau_collection": "TruthTaus",
            "uints_to_copy" : ["classifierParticleOutCome"],
            "max_delta_R": 0.3
        },
        {
            "block": "GNNAuxTaskMapper",
            "btagging_container": "BTagging_AntiKt4EMPFlow",
            "track_container": "InDetTrackParticles",
            "track_links": "GN2v01_TrackLinks",
            "track_aux_tasks": {
                "GN2v01_VertexIndex": "GN2v01_aux_VertexIndex",
                "GN2v01_TrackOrigin": "GN2v01_aux_TrackOrigin"
            }
        },
        {
            "block": "MultifoldTagger",
            "nn_paths": [
                "dev/BTagging/20240408/GN2v01NoAux/antikt4empflow/network_fold0.onnx",
                "dev/BTagging/20240408/GN2v01NoAux/antikt4empflow/network_fold1.onnx",
                "dev/BTagging/20240408/GN2v01NoAux/antikt4empflow/network_fold2.onnx",
                "dev/BTagging/20240408/GN2v01NoAux/antikt4empflow/network_fold3.onnx"
            ]
        },
        {
            "block": "MultifoldTagger",
            "nn_paths": [
                "dev/BTagging/20240627/GN2v01Lep/antikt4empflow/network_fold0.onnx",
                "dev/BTagging/20240627/GN2v01Lep/antikt4empflow/network_fold1.onnx",
                "dev/BTagging/20240627/GN2v01Lep/antikt4empflow/network_fold2.onnx",
                "dev/BTagging/20240627/GN2v01Lep/antikt4empflow/network_fold3.onnx"
            ]
        },
        {
            "block" : "SoftElectronsDecorator",
            "decorate_vars" : true,
            "decorate_truth_vars" : true,
            "link_electrons" : true
        }
    ]
}
